import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from mpl_toolkits import mplot3d

class linear_reg(object):
    def __init__(self,learning_rate=0.01,n_iter=20):
        self.learning_rate=learning_rate
        self.n_iter=n_iter

    def _fit(self,X,y):
        self.weights_=np.ones(1+X.shape[1])
        self.cost_=list()
        for i in range(self.n_iter):
            output=self.next_input(X)
            errors=(y-output)
            self.weights_[1:]+=self.learning_rate*X.T.dot(errors)
            self.weights_[0]+=self.learning_rate*errors.sum()
            cost=(errors**2).sum() / 2.0
            self.cost_.append(cost)
        return self

    def next_input(self,X):
        next_out=np.dot(X,self.weights_[1:]) + self.weights_[0]
        return next_out

    def predict(self,X):
        return self.next_input(X)

def main():

    path1="data_set/assign1_1.csv"
    path2="data_set/assign1_2.csv"

    data1=pd.read_csv(path1,header=None,sep=',')
    data2=pd.read_csv(path2,header=None,sep=',')
    data2.columns=['houses','bedrooms','price']
    data1.columns=['population','loss']
    X1=data1.iloc[:, :-1].values
    y1=data1['loss'].values
    X1=X1 / max(X1)
    y1=y1 / max(y1)

    X2=data2.iloc[:, :-1].values
    y2=data2['price'].values
    y2=1.0*y2 / max(y2)
    for i in range(X2.shape[1]):
        X2[:,i]=1.0*X2[:,i] / max(X2[:,i])

    lr=linear_reg()
    lr._fit(X2,y2)
    y2_new=lr.predict(X2)

    print('Solution for multi_variable model : ' )
    print('normalized [ intercept , weights  ] : ' +str(lr.weights_))
    plt.figure(1)
    plt.plot(range(1,lr.n_iter+1),lr.cost_)
    plt.xlabel('Epoch')
    plt.ylabel('Avg_Square Error')
    plt.title('Graph of MSE vs Epoch for multiple_class')
    plt.savefig('figure/cost_data2.png')

    x_new=list()
    y_new=list()
    z_new=list()
    for i in range(X2.shape[0]):
        x_new.append(X2[i,0])
        y_new.append(X2[i,1])
        z_new.append(y2[i])

    plt.figure(2)
    ax = plt.axes(projection='3d')
    ax.plot3D(x_new,y_new,y2_new,'black')
    ax.scatter(x_new,y_new,z_new,c='r',marker='o')
    plt.title('Regression for multiple_class')
    plt.savefig('figure/regression_data2.png')
    ax.set_xlabel('Normalized no of house')
    ax.set_ylabel('normalized Bedrooms')
    ax.set_zlabel('Normalized price')
    plt.show()


if __name__ == '__main__':
    main()

