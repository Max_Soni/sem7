import cv2 as cv
import numpy as np 
import matplotlib.pyplot as plt
class imresize :
    def __init__(self,path,factor):
        self.path=path
        self.factor=factor
    
    def read_image(self,type):
        image=cv.imread(self.path,type)
        return image

    def undersampling(self,image):
        size=np.shape(image)
        new_image_length=int(np.round(size[0]*self.factor))
        new_image_width=int(np.round(size[1]*self.factor))
        index1=np.sort(np.random.randint(0,size[0],size=new_image_length))
        index2=np.sort(np.random.randint(0,size[1],size=new_image_width))
        new_image=np.ones([1,(new_image_length)*(new_image_width)])[0]
        new_image=np.reshape(new_image,(new_image_length,new_image_width))

        for row in range(new_image_length):
            for col in range(new_image_width):
                new_image[row,col]+=image[index1[row],index2[col]]
        return new_image        


def main():
    path="images/redrose.jpeg"
    print("Enter factor")
    factor=input()
    factor=float(factor)
    z=imresize(path,factor)
    image=z.read_image(0)
    new_image=z.undersampling(image)
    plt.figure()
    plt.imshow(new_image,cmap="gray")
    plt.show()
    # show(new_image)
    

if __name__ == '__main__':
    main()


